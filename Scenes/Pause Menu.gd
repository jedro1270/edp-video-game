extends Node2D

func _on_Pause_Button_pressed():
	get_tree().paused = true
	visible = true
	get_parent().get_node("Pause Button").hide()

func _on_Continue_pressed():
	get_tree().paused = false
	visible = false
	get_parent().get_node("Pause Button").show()

func _on_Exit_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://Scenes/Title Page.tscn")
	if get_tree().root.has_node("Theme_Music"):
		get_tree().root.get_node("Theme_Music").queue_free()
	if get_tree().root.has_node("Day_2_Theme_Music"):
		get_tree().root.get_node("Day_2_Theme_Music").queue_free()
	if get_tree().root.has_node("Chase_Theme_Music"):
		get_tree().root.get_node("Chase_Theme_Music").queue_free()
	if get_tree().root.has_node("Credit Music"):
		get_tree().root.get_node("Credit Music").queue_free()
