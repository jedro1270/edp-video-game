extends RichTextLabel

signal end
var dialogue= Array()
var dialogue_index = 0

func _ready():
	set_visible_characters(0)
	$"Character Timer".stop()
	$"Next Line Timer".stop()
	

func _on_Character_Timer_timeout() -> void:
	set_visible_characters(get_visible_characters() + 1)

func _on_Next_Line_Timer_timeout():
	if get_visible_characters() > get_total_character_count():
		if dialogue_index == dialogue.size()-1:
			dialogue_index = 0
			$"Character Timer".stop()
			$"Next Line Timer".stop()
			set_visible_characters(0)
			set_bbcode(dialogue[dialogue_index])
			emit_signal("end")
		else:
			dialogue_index += 1
			set_bbcode(dialogue[dialogue_index])
			set_visible_characters(0)
