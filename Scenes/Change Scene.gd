extends Area2D

export(String, FILE, "*.tscn") var target_stage
export(String) var tag = "changeScene"

func _on_Change_Scene_body_entered(body):
	if "Player" in body.name:
		get_tree().change_scene(target_stage)
