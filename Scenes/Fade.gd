extends ColorRect

var startTime = 0
export var duration = 2000.0 # In milliseconds. 2000 is 2 seconds

export var fadeIn = false

signal finished

func _ready():
	set_process(false)

func _process(delta):
	var alpha = (OS.get_ticks_msec() - startTime) / duration
	alpha = clamp(alpha, 0, 1) # alpha value is only between 0 and 1
	
	if alpha == 1:
		emit_signal("finished")
		set_process(false)
	if fadeIn == true:
		alpha = 1 - alpha
	
	color.a = alpha # color.a is the alpha property

func start():
	color.a = 0
	startTime = OS.get_ticks_msec()
	set_process(true)
