extends Node

onready var tweenOut = get_node("TweenOut")

export var transitionDuration = 2
export var transitionType = 1 # TRANS_SINE
export var startingDb = 0

func fade_out(stream_player):
	# tween music volume down to 0
	tweenOut.interpolate_property(stream_player, "volume_db", startingDb, -80, transitionDuration, transitionType, Tween.EASE_IN, 0)
	tweenOut.start()
	# when the tween ends, the music will be stopped

func _on_TweenOut_tween_completed(object, key):
	# stop the music -- otherwise it continues to run at silent volume
	object.stop()
