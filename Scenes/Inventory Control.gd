extends Control

signal OnScreen_Ui(data)
signal OnScreen_Action(data)

var mode = 2
var bottles = 0
var newspapers = 0
var money = 0
var food = 0
onready var player_health = $"Player health bar"
onready var enemy_health  =$"Emeny health bar"

func _process(delta):
	gui_mode(mode)
	if global.day2 == false:
		$"Bottle/Bottle Counter".text = String(global.player.collected_Items.bottles)
		$"Newspaper/Newspaper Counter".text = String(global.player.collected_Items.newspapers)
		$"Money/Money Counter".text = String(global.player.money)
		$"Food/Food Counter".text = String(global.player.foodServing) + " servings"
	else:
		$Bottle.hide()
		$Newspaper.hide()
		$Money.hide()
		$Food.hide()

func gui_mode(mode):
	match mode:
		1:
			$Food.hide()
			$Bottle.hide()
			$Newspaper.hide()
			$Money.hide()
			$"Player health bar".show()
			$"Emeny health bar".show()
		2:
			if global.day2 == false:
				$Food.show()
				$Bottle.show()
				$Newspaper.show()
				$Money.show()
				$"Player health bar".hide()
				$"Emeny health bar".hide()


func touch_on_pressed(data):
	emit_signal("OnScreen_Ui",data)
	
func update_health(player,enemy):
	enemy_health.value = enemy
	player_health.value = player


func _on_Pickable_Item_collected_items():
	randomize()
	global.player.collected_Items.bottles += randi()%10 + 1
	global.player.collected_Items.newspapers += randi()%10 + 1


func _on_Pickable_Item2_collected_items():
	randomize()
	global.player.collected_Items.bottles += randi()%10 + 1
	global.player.collected_Items.newspapers += randi()%10 + 1


func _on_Pickable_Item3_collected_items():
	randomize()
	global.player.collected_Items.bottles += randi()%10 + 1
	global.player.collected_Items.newspapers += randi()%10 + 1


func _on_Pickable_Item4_collected_items():
	randomize()
	global.player.collected_Items.bottles += randi()%20 + 1
	global.player.collected_Items.newspapers += randi()%20 + 1

func _on_Pickable_Item5_collected_items():
	randomize()
	global.player.collected_Items.bottles += randi()%10 + 1
	global.player.collected_Items.newspapers += randi()%10 + 1


func _on_Action_pressed():
	emit_signal("OnScreen_Action","Action")


func _on_Action_released():
	emit_signal("OnScreen_Action","Stop")
