extends Node2D

signal end

export(int) var charPerSeconds = 15
export(Array,String) var dialogue = ["testing, one two three!","speech test","okay, looks good!","hi"]
onready var player_Dialogue = $"Cutscene Dialogue/Player Dialogue Text"
onready var cutsceneDialogue = $"Cutscene Dialogue"
onready var character_Timer = $"Cutscene Dialogue/Player Dialogue Text/Character Timer"
onready var nextLine_Timer = $"Cutscene Dialogue/Player Dialogue Text/Next Line Timer"
onready var button = $"debug_button(for testing)"
var speaking = false


func _process(delta):
	if button.pressed:
		start(0,2)


func _ready():
	cutsceneDialogue.hide()
	character_Timer.wait_time = (1/charPerSeconds as float)

func _on_Player_Dialogue_Text_end():
	cutsceneDialogue.hide()
	false
	emit_signal("end")
	
func start(start, end):
	var temp = Array()
	for n in range(start,end):
		temp.append(dialogue[n])
	speaking = true
	player_Dialogue.dialogue = temp
	player_Dialogue.set_bbcode(temp[0])
	character_Timer.start(0)
	nextLine_Timer.start(0)
	cutsceneDialogue.show()
