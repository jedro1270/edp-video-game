extends Area2D

func _physics_process(delta):
	var item = false
	
	for area in get_overlapping_areas():
		match  area.collision_layer:
			4:
				item =  true
	
	$"Question Mark".visible = item