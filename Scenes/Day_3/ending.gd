extends Node2D


var speaking = false 
func _ready() -> void:
	$"Player Thoughts".start(0, 6)


func _on_Player_Thoughts_end():
	get_tree().change_scene("res://Scenes/Credits.tscn")
