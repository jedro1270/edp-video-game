extends Node2D

func _on_Area2D_body_entered(body: PhysicsBody2D) -> void:
	if body.name == "actor":
		print("player enter")
		$actor.canMove = false
		$Fade.show()
		$Fade.start()
		$"Fade Music".fade_out(get_tree().root.get_node("Chase_Theme_Music"))
		
	if body.name == "police":
		$police/AnimatedSprite.play()
		$police.canMove = false


func _on_Fade_finished():
	get_tree().root.get_node("Chase_Theme_Music").queue_free()
	get_tree().change_scene("res://Scenes/Day_3/ending.tscn")
	get_tree().root.call_deferred("add_child", load("res://Scenes/Credit Music.tscn").instance())
