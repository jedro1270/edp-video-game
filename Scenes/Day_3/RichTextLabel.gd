extends RichTextLabel

signal end
var dialogue_index = 0

var dialogue =["After I was caught by the police, I confessed everything" , "Including all the information I could give about that organization.",
"The police were able to rescue my sister when I told them where she was assigned to beg.",
"Unfortunately, they were not able to catch the organization."]

func _ready():
	set_visible_characters(0)

func _on_Character_Timer_timeout() -> void:
	set_visible_characters(get_visible_characters() + 1)

func _on_Next_Line_Timer_timeout():
	
	if get_visible_characters() > get_total_character_count():
		if dialogue_index == dialogue.size()-1:
			
			dialogue_index = 0
			$"../../Character Timer".stop()
			set_visible_characters(0)
			set_bbcode(dialogue[dialogue_index])
			emit_signal("end")
		else:
			dialogue_index += 1
			set_bbcode(dialogue[dialogue_index])
			set_visible_characters(0)
