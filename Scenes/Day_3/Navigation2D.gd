extends Navigation2D

export(float) var character_speed = 100.0
var path = []

func _process(delta):
	var walk_distance = character_speed * delta
#	_update_navigation_path($Enemy.position, global.player.position)
	move_along_path(walk_distance)


# The 'click' event is a custom input action defined in
# Project > Project Settings > Input Map tab.
func _input(event):
	if not event is InputEventMouseButton:
		return
	_update_navigation_path($Enemy.position, get_local_mouse_position())



func move_along_path(distance):
	var last_point = $Enemy.position
	
	if path.size():
		var distance_between_points = last_point.distance_to(path[0])
		
		$Enemy.path = path[0]
		
		if distance <= distance_between_points:
			if $Enemy.position.x <=  path[0].x:
				$Enemy.set_dir(1)
			elif $Enemy.position.x >=  path[0].x:
				$Enemy.set_dir(-1)
			else:
				print("stop")
				path.remove(0)
				$Enemy.set_dir(0)
				set_process(false)
		distance -= distance_between_points
		
#	while path.size():
#		var distance_between_points = last_point.distance_to(path[0])
#		print("path" + String(path[0]))
#		# The position to move to falls between two points.
#		print(distance_between_points)
#		if distance <= distance_between_points:
#			$Enemy.position = last_point.linear_interpolate(path[0], distance / distance_between_points)
#			return
#		# The position is past the end of the segment.
#		distance -= distance_between_points
#		last_point = path[0]
#		path.remove(0)
#	# The character reached the end of the path.
#	$Enemy.position = last_point
#	print(global.player.position)
#	print($Enemy.position)
#	set_process(false)


func _update_navigation_path(start_position, end_position):
	# get_simple_path is part of the Navigation2D class.
	# It returns a PoolVector2Array of points that lead you
	# from the start_position to the end_position.
	path = get_simple_path(start_position, end_position, true)
	$Line2D.points = path
	# The first point is always the start_position.
	# We don't need it in this example as it corresponds to the character's position.
	path.remove(0)
	set_process(true)


func _on_Timer_timeout():
	path = get_simple_path($Enemy.position, global.player.position, true)
	$Line2D.points = path
	path.remove(0)
	set_process(true)
