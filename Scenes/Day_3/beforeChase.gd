extends Node2D

func _ready():
	get_tree().root.call_deferred("add_child", load("res://Scenes/Day_3/Chase_Theme_Music.tscn").instance())
	$FadeIn.show()
	$FadeIn.start()

func _on_otherScene_body_entered(body: PhysicsBody2D) -> void:
	if body.name == "actor":
		$pickpocket.show()
		$actor.canMove = false
		$Fade.show()
		$Fade.start()
		

func _on_Fade_finished() -> void:
	get_tree().change_scene("res://Scenes/Day_3/afterSnatch.tscn")
