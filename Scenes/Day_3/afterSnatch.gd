extends Node2D

func _ready() -> void:
	pass 

func _on_portalGame_body_entered(body: PhysicsBody2D) -> void:
	if body.name == "police":
		$Fade.show()
		$Fade.start()


func _on_Fade_finished():
	get_tree().change_scene("res://Scenes/Day_3/chase.tscn")
