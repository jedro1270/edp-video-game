extends KinematicBody2D

export var speed = 400
var direction = 1
var velocity = Vector2()
export var canMove = false
var whistled = false

func _physics_process(delta: float) -> void:
	if canMove:
		velocity.x += direction
		velocity = velocity.normalized() * speed
		
		position += velocity * delta
		$AnimatedSprite.play("running")
	else:
		$AnimatedSprite.play("idle")

func _on_Timer_timeout() -> void:
	canMove = true
	if whistled == false:
		$Whistle.play()
		whistled = true
