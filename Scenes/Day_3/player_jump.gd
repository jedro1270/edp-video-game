extends KinematicBody2D

var stamina = 70
export var speed = 500
export var gravity = 1800
export var jumpSpeed = 600
var storedG = gravity
var velocity = Vector2()
var UP = Vector2(0,-1)
var caught = false
var ui_data
var ui_action
export var canMove = true

func _physics_process(delta: float) -> void:
	if canMove == true:
		global.player.position = position
		var onLadder : = false
		
		for area in $Area2D.get_overlapping_bodies():
			onLadder = area.collision_layer == 16
			
		
		stamina = clamp(stamina,0,70)
		
		if stamina > 0 and not caught:
			$AnimatedSprite.speed_scale = stamina/70
			speed = 500 * stamina/70
				
			if Input.is_action_pressed("ui_left") or ui_data == "Left":
				velocity.x = -speed
				
				if is_on_floor():
					$AnimatedSprite.flip_h = true
					$AnimatedSprite.play("running")
					stamina -= 0.1
					
			elif Input.is_action_pressed("ui_right") or ui_data == "Right":
				velocity.x = +speed
				
				if is_on_floor():
					$AnimatedSprite.flip_h = false
					$AnimatedSprite.play("running")
					stamina -= 0.1
				
			else:
				velocity.x = 0
			
			if is_on_floor() and !onLadder and (Input.is_action_pressed("ui_up") or ui_action == "Action"):
				velocity.y = -jumpSpeed
				stamina -= 3
				$AnimatedSprite.play("jump")
			
			elif onLadder and (Input.is_action_pressed("ui_up") or ui_action == "Action"):
				$AnimatedSprite.play("climbing")
				gravity = 0
				velocity.y = -speed
				stamina -= 0.2
			
			else:
				gravity = storedG
			
			velocity.y += gravity*delta
	
			if is_on_floor() and velocity.y > 0:
				velocity.y = 0
			
			if velocity.x == 0 and velocity.y == 0:
				$AnimatedSprite.play("idle")
			
			velocity = move_and_slide(velocity, UP)
	
		else:
			velocity.x = 0
			global.GameOver()
			$AnimatedSprite.play("idle")

func _on_water_body_entered(body: PhysicsBody2D) -> void:
	print("water")
	print("+5 stamina")
	stamina += 30

func _on_Area2D_body_entered(body):
	if body.name == "Enemy":
		global.gameOver = true


func _on_Control_OnScreen_Ui(data):
	ui_data = data


func _on_Control_OnScreen_Action(data):
	ui_action = data
