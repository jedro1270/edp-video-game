extends Node2D

func _ready():
	$Player.canMove = false
	$Navigation2D/Enemy.canMove = false

func _process(delta) -> void:
	if global.gameOver == true:
		$"Navigation2D/Enemy".canMove = false
		$Player.canMove = false
	$"Player/staminaBar/playerBar".value = $"Player".stamina


func _on_Timer_timeout():
	$CanvasLayer/ColorRect.hide()
	$Player.canMove = true
	$Navigation2D/Enemy.canMove = true
	$Navigation2D/Enemy.canMove = true
	$Navigation2D/Timer.start()
