extends Node2D


func _ready():
	global.day2 = true
	global.save_scene()
	$FadeIn.show()
	$FadeIn.start()
	$"Player Thoughts".start(0, 3)
	$Player.canMove = false

func _on_Beginning_Suspense_Music_finished():
	$Player.canMove = true
	$day3.hide()
	$AcceptDialog.popup()
	$"Consistent Music".play(0)


func _on_Player_Thoughts_end():
	$FadeIn2.show()
	$FadeIn2.start()


func _on_Dialogue_Trigger_body_entered(body):
	$Player.canMove = false
	$Player.speak_dialogue(0, 2)
	
func _on_Player_dialogue_end():
	$"Black Out".show()
	$"guard/Whack".play()


func _on_Whack_finished():
	$"Black Out".start()
	$"guard/Guard Dialogue".start(0, 1)


func _on_Guard_Dialogue_end():
	$FadeOut.show()
	$FadeOut.start()

func _on_FadeOut_finished():
	$"FadeOut".hide()
	$"Player Thoughts End".show()
	$"Player Thoughts End".start(0, 3)


func _on_Player_Thoughts_End_end():
	get_tree().change_scene("res://Scenes/Day_3/beforeChase.tscn")
