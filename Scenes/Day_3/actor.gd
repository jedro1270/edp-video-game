extends KinematicBody2D

export var speed = 400
var direction = 1
var velocity = Vector2()
export var canMove = true

func _physics_process(delta: float) -> void:
	if canMove:
		velocity.x += direction
		velocity = velocity.normalized() * speed
		
		position += velocity * delta
		$AnimatedSprite.play("run")
	else:
		$AnimatedSprite.play("idle")
