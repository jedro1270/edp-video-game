extends Area2D

func _process(delta: float) -> void:
	for body in get_overlapping_bodies():
		if body.name == "Player":
			queue_free()
