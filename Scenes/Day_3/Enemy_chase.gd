extends KinematicBody2D

onready var Player = get_parent().get_parent().get_node("Player")
onready var EdgeDetection = $"Edge detection"
onready var ObjectInFront  =$"Object in front"
export var speed  = 400
var vel = Vector2(0, 0)
var grav = 1800
var max_grav = 3000
var storedG = grav
var react_time = 400
var path := Vector2()
var dir = 0
var next_dir = 0
var next_dir_time = 0
var next_jump_time = -1
var target_player_dist = 5
export var canMove = true


#func _ready():
#	set_physics_process(false)

func set_dir(target_dir):
	if canMove == true:
		if next_dir != target_dir:
			next_dir = target_dir
			next_dir_time = OS.get_ticks_msec() + react_time
		if target_dir == -1:
			$AnimatedSprite.flip_h = true
			$AnimatedSprite.play("walking")
		if target_dir == 1:
			$AnimatedSprite.flip_h = false
			$AnimatedSprite.play("walking")

func _physics_process(delta):
	if canMove == true:
	
		var onLadder : = false
	#
		for area in $Area2D.get_overlapping_bodies():
			onLadder = area.collision_layer == 16
	
		_sensors()
	#
	#		if Player.position.x < position.x - target_player_dist :
	#			set_dir(-1)
	#			EdgeDetection.cast_to = Vector2(-13,19)
	#			ObjectInFront.cast_to = Vector2(-15,0)
	#			$AnimatedSprite.flip_h = true
	#			$AnimatedSprite.play("walking")
	#		elif Player.position.x > position.x + target_player_dist :
	#			set_dir(1)
	#			EdgeDetection.cast_to = Vector2(13,19)
	#			ObjectInFront.cast_to = Vector2(15,0)
	#			$AnimatedSprite.flip_h = false
	#			$AnimatedSprite.play("walking")
	#		else:
	#			set_dir(0)
	#			$AnimatedSprite.stop()
				
		if OS.get_ticks_msec() > next_dir_time:
			dir = next_dir
			
		if OS.get_ticks_msec() > next_jump_time and next_jump_time != -1 and is_on_floor() and ObjectInFront.is_colliding() and !onLadder:
			if Player.position.y < position.y - 64:
				vel.y = -1000
			next_jump_time = -1
		
		vel.x = dir * speed
		
		if onLadder:
			grav = 0
			if path.y < position.y:
				vel.x = 0
				vel.y = -speed
			else:
				vel.y = 0
		else:
	
			grav = storedG
		
		if Player.position.y < position.y - 64 and next_jump_time == -1:
			next_jump_time = OS.get_ticks_msec() + react_time
		
		vel.y += grav * delta
		
		if vel.y > max_grav:
			vel.y = max_grav
		
		if is_on_floor() and vel.y > 0:
			vel.y = 0
		
		vel = move_and_slide(vel, Vector2(0, -1))
		

func _sensors():
	if EdgeDetection.is_colliding():
		pass
#		print(EdgeDetection.get_collider().collision_layer)


