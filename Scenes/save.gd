extends Node

var saved_scene = null

func save_scene():
	saved_scene = get_tree().get_current_scene()

func load_scene():
	if saved_scene != null:
		var root = get_tree().get_root()
		var current_scene = get_tree().get_current_scene()
	 #   root.remove_child(current_scene)
		current_scene.queue_free()
		root.add_child(saved_scene)
		saved_scene = null
