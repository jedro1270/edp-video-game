extends OptionButton

signal bought

func _ready():	
	add_separator()
	add_item("Chicken adobo with rice:    Php 50")
	add_item("Lechon manok:    Php 150")
	add_item("Pork sisig with rice:    Php 50")
	add_item("Palabok:    Php 30")
	
	if global.player.money < 150:
		set_item_disabled(2, true)
		
	if global.player.money < 50:
		set_item_disabled(1, true)
		set_item_disabled(3, true)

	if global.player.money < 30:
		set_item_disabled(4, true)

func _on_OptionButton_item_selected(ID):
	if ID == 4:
		global.player.money -= 30
	elif ID == 1 or ID == 3:
		global.player.money -= 50
	elif ID == 2: 
		global.player.money -= 150
		
	global.player.foodServing += 1
	emit_signal("bought")
