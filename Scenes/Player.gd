extends KinematicBody2D

signal Action
signal Punch(damage)
signal dialogue_end
signal running
signal jumping

var screen_size
var toggle = 1
var onScreen_Ui
var actions = ["running","pick up","punch"]
var facing = 0
var speaking
var canPunch := true
export var screen_restrict = false
export var speed = 100
export var damage = 5
export var health = 100
export var canMove = true
export var actionState = 1
export var collected_items = {"Bottles":0,"NewsPapers":0}
export(Array,String) var dialogues = []
onready var player_Dialogue = $"Player Dialogue"
onready var punch = $pivot/punch/CollisionShape2D

func _ready():
	screen_size = get_viewport_rect().size

func _physics_process(delta):
	speaking = $"Player Dialogue".speaking
	global.player.location = position.x
	player_movement(canMove,delta)

func player_movement(enable,delta):
	
	var velocity = Vector2()
	
	if enable:
		
		if Input.is_action_pressed("ui_right") or onScreen_Ui == "Right":
			$pivot.rotation_degrees = 0
			$AnimatedSprite.flip_h = false
			$AnimatedSprite.play("walking")
			velocity.x += 1
			facing = 1
		elif Input.is_action_pressed("ui_left") or onScreen_Ui == "Left":
			$pivot.rotation_degrees = 180
			$AnimatedSprite.flip_h = true
			$AnimatedSprite.play("walking")
			velocity.x -= 1
			facing = -1
		elif Input.is_action_pressed("ui_down") or onScreen_Ui == "Action":
			interaction()
			emit_signal("Action")
		else:
			toggle = true
			punch.disabled = true
			$AnimatedSprite.animation = "idle"
			$AnimatedSprite.stop()
			$"Walking Sound".stop()
			
		if velocity.length() > 0:
			velocity = velocity.normalized() * speed
			if not $"Walking Sound".playing:
				$"Walking Sound".playing = true
			
	else:
		punch.disabled = true
		$AnimatedSprite.animation = "idle"
		$AnimatedSprite.stop()
		$"Walking Sound".stop()
		
	move_and_slide(velocity)
	position += velocity * delta
	
	if screen_restrict == true:
		position.x = clamp(position.x, 0, screen_size.x)

func update_Collected_Items(bottles, newspaper):
	collected_items.Bottles = bottles
	collected_items.NewsPapers = newspaper
	$"Pick Up Sound".play()

func interaction():
	if !punch.disabled:
		punch.disabled = true
	
	if toggle:
		$AnimatedSprite.play(actions[actionState])
		if canPunch && actionState == 2:
			$threshold.start()
			canPunch = false
			punch.disabled = false

func speak_dialogue(start:int,end:int):
	speaking = true
	player_Dialogue.dialogue = dialogues
	player_Dialogue.start(start,end)

func _on_Control_OnScreen_Ui(data):
	onScreen_Ui  = data


func _on_punch_body_entered(body):
	if body.name == "Enemy":
		emit_signal("Punch",damage)


func _on_Enemy_Punch(damage: int, facing: int):
	health-= damage
	$"Player Hit Sound".play()
	print("Enemy hit Player! " + String(damage))
	translate(Vector2(50*-facing, 0))
	
func _on_Player_Dialogue_end():
	emit_signal("dialogue_end")
	speaking = false

func _on_threshold_timeout():
	canPunch = true

func _on_Control_OnScreen_Action(data):
	onScreen_Ui = data
