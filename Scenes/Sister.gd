extends KinematicBody2D

var screen_size
var onScreen_Ui
var facing = 0
export var screen_restrict = false
export var speed = 100
export var canMove = true
export var actionState = 1

func _ready():
	screen_size = get_viewport_rect().size

func _physics_process(delta):
	if global.day2 == true:
		global.player.location = position.x
		player_movement(canMove,delta)

func player_movement(enable,delta):
	
	var velocity = Vector2()
	
	if enable:
		
		if Input.is_action_pressed("ui_right") or onScreen_Ui == "Right":
			$AnimatedSprite.flip_h = false
			$AnimatedSprite.play("walking")
			velocity.x += 5
			facing = 1
		elif Input.is_action_pressed("ui_left") or onScreen_Ui == "Left":
			$AnimatedSprite.flip_h = true
			$AnimatedSprite.play("walking")
			velocity.x -= 5
			facing = -1
		else:
			$AnimatedSprite.animation = "idle"
			$AnimatedSprite.stop()
			
		if velocity.length() > 0:
			velocity = velocity.normalized() * speed
			
	else:
		$AnimatedSprite.animation = "idle"
		$AnimatedSprite.stop()
		
	move_and_slide(velocity)
	position += velocity * delta
	
	if screen_restrict == true:
		position.x = clamp(position.x, 0, screen_size.x)

func _on_Control_OnScreen_Ui(data):
	onScreen_Ui  = data
