extends Node

signal no_loaded_game

onready var currentScene = get_tree().get_root().get_child(
			get_tree().get_root().get_child_count() - 1
		)

func change_scene(path):
	currentScene.queue_free() # Deletes the scene
	currentScene = load(path).instance()
	get_tree().get_root().add_child(currentScene)


#player components
var player = {'location':Vector2(),'health': 100,'collected_Items':{'bottles': 0, 'newspapers': 0},'money':0,'foodServing':0}
var cutscene = false # First part is a cutscene
var junkyard_scene_finished = false
var bought_food = false
var restaurant_scene = false
var day2 = false
var scene_location
var gameOver = false

# For saving

var saved_scene = null

func save_scene():
	saved_scene = get_tree().get_current_scene().filename
	var save_game = File.new()
	
	if save_game.file_exists("user://savegame.save") == true:
		var dir = Directory.new()
		dir.remove("user://savegame.save")
		
	save_game.open("user://savegame.save", File.WRITE)
	save_game.store_line(to_json(saved_scene))
	save_game.close()

func load_scene():
	var save_game = File.new()
	
	if save_game.file_exists("user://savegame.save") == false:
		emit_signal("no_loaded_game")
	else:	
		save_game.open("user://savegame.save", File.READ)
		get_tree().change_scene(parse_json(save_game.get_line()))
		save_game.close()
