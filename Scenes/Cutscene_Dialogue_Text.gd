extends RichTextLabel

var dialogue
var page = 0 
var nextPage

signal finished

func _ready():
	set_bbcode(dialogue[page])
	set_visible_characters(0)
	set_process_input(true)

func _input(event):
	if nextPage == true:
		nextPage = false
		if get_visible_characters() > get_total_character_count():
			if page < dialogue.size() - 1 :
				page += 1
				set_bbcode(dialogue[page])
				set_visible_characters(0)
			elif page == dialogue.size() - 1:
				emit_signal("finished")
	
func _on_Timer_timeout():
	set_visible_characters(get_visible_characters() + 1)
	


func _on_Page_Timer_timeout():
	nextPage = true
