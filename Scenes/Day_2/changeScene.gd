extends Timer

signal endScene 

func _ready() -> void:
	pass # Replace with function body.

func _on_dialogue_end() -> void:
	get_parent().add_child(load("res://Scenes/Fade.tscn").instance())
	emit_signal("endScene")
	
	#get_tree().change_scene("res://Scenes/day2/rainChurch.tscn")
