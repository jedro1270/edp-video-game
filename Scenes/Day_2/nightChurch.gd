extends Node2D


func _ready():
	$FadeIn.show()
	$FadeIn.start()


func _on_FadeIn_finished():
	$"Cutscene Dialogue".start(0, 2)

func _on_Cutscene_Dialogue_end():
	$FadeOut.show()
	$FadeOut.start()
	$"Fade Music".fade_out(get_tree().root.get_node("Day_2_Theme_Music"))


func _on_FadeOut_finished():
	get_tree().change_scene("res://Scenes/Day_3/Criminal Room.tscn")
