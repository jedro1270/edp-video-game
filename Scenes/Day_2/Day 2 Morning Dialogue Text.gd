extends RichTextLabel

var dialogue = ["Good morning Manong! I feel all better now!","Hahaha alright Lina, calm down, let’s go to the church to celebrate."]

var page = 0
var nextPage = false

signal end

func _ready():
	set_bbcode(dialogue[page])
	set_visible_characters(0)

func _process(delta):
	if nextPage == true:
		nextPage = false
		if get_visible_characters() > get_total_character_count():
			if page < dialogue.size() - 1 :
				page += 1
				set_bbcode(dialogue[page])
				set_visible_characters(0)
				scroll_to_line(page)
			elif page == dialogue.size() - 1:
				emit_signal("end")

func _on_Character_Timer_timeout() -> void:
	set_visible_characters(get_visible_characters() + 1)


func _on_Next_Line_Timer_timeout():
	nextPage = true
