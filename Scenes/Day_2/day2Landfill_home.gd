extends Node2D

func _ready():
	global.day2 = true
	$FadeIn.show()
	$FadeIn.start()
	global.player.foodServing = 0

func _on_Change_Scene_body_entered(body):
	$FadeOut.show()
	$FadeOut.start()
	$Player.canMove = false
	$Sister.canMove = false

func _on_FadeOut_finished():
	get_tree().change_scene("res://Scenes/Day_2/fineWeather.tscn")
