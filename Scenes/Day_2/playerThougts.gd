extends Polygon2D

signal changeScene

func _ready() -> void:
	pass # Replace with function body.



func _on_RichTextLabel_end() -> void:
	add_child(load("res://Scenes/Fade.tscn").instance())
	emit_signal("changeScene")


func _on_playerThoughts_changeScene() -> void:
	get_tree().change_scene("res://Scenes/day2/nightChurch.tscn")
