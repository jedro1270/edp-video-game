extends Sprite

func _ready():
	$Player/AnimatedSprite.flip_h = true
	$FadeIn.show()
	$FadeIn.start()

func _on_FadeIn_finished():
	$"Player Dialogue".start(0, 6)

func _process(delta):
	if $"Player Dialogue/Speech Bubble/Player Dialogue Text".dialogue_index == 1 or $"Player Dialogue/Speech Bubble/Player Dialogue Text".dialogue_index >= 3:
		$"Player Dialogue/Speech Bubble".flip_h = true
	else:
		$"Player Dialogue/Speech Bubble".flip_h = false
