extends Node2D

var fade

func _ready():
	global.save_scene()
	if get_tree().root.has_node("Theme_Music") == false:
		get_tree().root.call_deferred("add_child", load("res://Scenes/Theme_Music.tscn").instance())
	$Player.canMove = false
	$"Fade In".show()
	$"Fade In".start()
	$Player/AnimatedSprite.flip_h = true

func _process(delta):
	if $"Player Dialogue/Speech Bubble/Player Dialogue Text".dialogue_index >= 2:
		$"Player Dialogue/Speech Bubble".flip_h = false

func _on_Player_Dialogue_end():
	$"Fade Out".show()
	$"Fade Out".start()

func _on_Fade_Out_finished():
	get_tree().change_scene("res://Scenes/Day_2/day2Landfill_home.tscn")


func _on_Day_2_Label_Timer_timeout():
	$"day2".hide()

func _on_Fade_In_finished():
	$"Player Dialogue".start(0, 4)
	$AcceptDialog.popup()
