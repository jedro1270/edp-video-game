extends Node2D

func _ready():
	get_tree().root.call_deferred("add_child", load("res://Scenes/Day_2/Day_2_Theme_Music.tscn").instance())
	$FadeIn.show()
	$FadeIn.start()
	$"Cutscene Dialogue".start(0, 2)
	$Player.canMove = false
	$Sister.canMove = false

func _on_Enter_Church_body_entered(body):
	$FadeOut.show()
	$FadeOut.start()
	$Player.canMove = false
	$Sister.canMove = false

func _on_FadeOut_finished():
	get_tree().change_scene("res://Scenes/Day_2/inside church.tscn")

func _on_Cutscene_Dialogue_end():
	$Player.canMove = true
	$Sister.canMove = true
