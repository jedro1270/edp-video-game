extends Sprite

func _ready():
	$Player/AnimatedSprite.flip_h = true
	$FadeIn.show()
	$FadeIn.start()

func _on_FadeIn_finished():
	$"Player Dialogue".start(0, 7)

func _process(delta):
	if $"Player Dialogue/Speech Bubble/Player Dialogue Text".dialogue_index == 1 or $"Player Dialogue/Speech Bubble/Player Dialogue Text".dialogue_index >= 3:
		$"Player Dialogue/Speech Bubble".flip_h = true
	else:
		$"Player Dialogue/Speech Bubble".flip_h = false

func _on_Player_Dialogue_end():
	$FadeOut.show()
	$FadeOut.start()

func _on_FadeOut_finished():
	$"Player Thoughts".show()
	$"Player Thoughts".start(0, 5)


func _on_Player_Thoughts_end():
	print("end")
	get_tree().change_scene("res://Scenes/Day_2/nightChurch.tscn")
