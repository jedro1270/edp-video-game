extends RichTextLabel

var dialogue = ["Let’s sleep here for now, we should be safe sleeping near a church"]

var page = 0

func _ready():
	set_bbcode(dialogue[page])
	set_visible_characters(0)

func _process(delta):
		if get_visible_characters() > get_total_character_count():
			if page < dialogue.size() - 1 :
				page += 1
				set_bbcode(dialogue[page])
				set_visible_characters(0)
				scroll_to_line(page)
			elif page == dialogue.size() - 1:
				pass

func _on_Timer_timeout() -> void:
		set_visible_characters(get_visible_characters() + 1)



