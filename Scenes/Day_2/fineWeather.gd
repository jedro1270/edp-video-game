extends Node2D

func _ready():
	$FadeIn.show()
	$FadeIn.start()
	$"Player Dialogue".start(0, 2)
	

func _on_Player_Dialogue_end():
	$FadeOut.show()
	$FadeOut.start()
	$"Fade Music".fade_out(get_tree().root.get_node("Theme_Music")) # SHould be Theme_Music

func _on_FadeOut_finished():
	get_tree().change_scene("res://Scenes/Day_2/rainChurch.tscn")
	get_tree().root.get_node("Theme_Music").queue_free()
