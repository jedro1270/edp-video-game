extends Node2D

var vidLength = 42 # In seconds
onready var skip_button  = $CanvasLayer/Button
var fading = false

func _ready():
	skip_button.hide()
	if get_tree().root.has_node("Credit Music") == false:
		$"Credit Music".play(2)

func _input(event):
#	if not event is InputEventScreenTouch:
#		return
	if event.is_pressed():
		_pressed_detection()


func _process(delta):
	if $"CanvasLayer/Credits".get_stream_position() >= 40 and fading == false:
		$"Fade Music".fade_out($"CanvasLayer/Credits")
		fading = true
	if $"CanvasLayer/Credits".get_stream_position() >= vidLength or skip_button.pressed:
		$"Credit Music".stop()
		if get_tree().root.has_node("Credit Music"):
			get_tree().root.get_node("Credit Music").queue_free()
			
		get_tree().change_scene("res://Scenes/Title Page.tscn")

func _pressed_detection():
	skip_button.show()
