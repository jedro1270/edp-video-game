extends CanvasLayer

onready var banner = $background
onready var timer = $Timer

signal game_over

func _ready():
	banner.hide()

func _process(delta):
	if global.gameOver:
		banner.show()
		if timer.is_stopped():
			timer.start()

func _on_Timer_timeout():
	global.gameOver = false	
	get_tree().reload_current_scene()
	set_process(false)
