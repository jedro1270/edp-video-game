extends Node2D

func _ready():
	$CanvasLayer/FadeIn.show()
	$CanvasLayer/FadeIn.start()


func _on_FadeIn_finished():
	$Timer.start()


func _on_FadeOut_finished():
	get_tree().change_scene("res://Scenes/Title Page.tscn")


func _on_Timer_timeout():
	$CanvasLayer/FadeOut.show()
	$CanvasLayer/FadeOut.start()
