extends VideoPlayer

var vidLength = 34 # In seconds


func _ready():
	$Button.hide()

func _input(event):
	if event.is_pressed():
		_pressed_detection()


func _process(delta):
	if get_stream_position() >= vidLength:
		_on_Button_pressed()

func _pressed_detection():
	$Button.show()


func _on_Button_pressed():
	get_tree().change_scene("res://Scenes/Day_1/Landfill_home.tscn")
