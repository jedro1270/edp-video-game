extends Popup


func _on_Pickable_Objects_body_entered(body):
	popup_centered()


func _on_Pickable_Objects_body_exited(body):
	hide()
