extends Node2D


func _ready():
	$FadeIn.show()
	$FadeIn.start()
		
	# ratio of bottles per peso is Php 22/50 bottles = 0.44 or Php 0.4 each
	# ratio of newspapers per peso is Php 10/35 newspapers = 0.28 or Php 0.3 each
	var money_from_bottles = round((global.player.collected_Items.bottles) * 0.4)
	var money_from_newspapers = round((global.player.collected_Items.newspapers) * 0.3)
	global.player.money += money_from_bottles + money_from_newspapers
	global.player.collected_Items.bottles = 0
	global.player.collected_Items.newspapers = 0
	
	if global.player.money >= 30:
		$Player.speak_dialogue(0, 1)
		$Player.canMove = false
	else:
		$Player.speak_dialogue(1, 3)
		$Player.canMove = false

func _on_Player_dialogue_end():
	$Player.canMove = true

