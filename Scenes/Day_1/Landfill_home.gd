extends Node2D

var screen_Lenth = 1440
var left_Limit
var right_limit 
var camera_moved =false
var Entered_bully_area = false
var count = 0
var scene_index = -1
var camera_speed = 100
var walkable_area_limit = 4178
var speak_index = 0
onready var player = $Player
onready var fadeDelay = $Fade_delay
onready var fade = $Fade
onready var control = $Position2D/Control
onready var Enemy = $Enemy
onready var enemy_dialogue = $"Enemy/Player Dialogue"

func _ready():
	global.save_scene()
	get_tree().root.call_deferred("add_child", load("res://Scenes/Theme_Music.tscn").instance())
	$Fade2.hide()
	player.canMove = false
	player.speak_dialogue(0,1)
	fade.rect_position = Vector2(0,0)
	fadeDelay.wait_time = 2
	fadeDelay.start()
	$Sister/AnimatedSprite.animation = "sleep"
	left_Limit = $Position2D.position.x
	right_limit  = $Position2D.position.x + screen_Lenth

func _physics_process(delta):
	
	if Input.is_action_pressed("ui_page_up"):
		$Position2D.position.x += 10
	if Input.is_action_pressed("ui_page_down"):
		$Position2D.position.x -= 10
	scene(scene_index)

func update_camera_postion(speed):
	var remainder = screen_Lenth%speed
	var loop_count = floor(screen_Lenth/speed)
	
	if $Player.position.x > right_limit:
		if count < loop_count:
			$Position2D.position.x += speed
			count+=1
		else:
			$Position2D.position.x += remainder
			right_limit = $Position2D.position.x + screen_Lenth
			left_Limit = $Position2D.position.x
			count = 0
			camera_moved = true
			
	elif $Player.position.x < left_Limit:
		if count < loop_count:
			$Position2D.position.x -= speed
			count+=1
		else:
			$Position2D.position.x -= remainder
			right_limit = $Position2D.position.x + screen_Lenth
			left_Limit = $Position2D.position.x
			count = 0
			camera_moved = true
			
	else:
		camera_moved = false

func _on_Timer_timeout():
	scene_index +=1
	player.canMove = false
	fade.start()


func _on_Area2D_area_entered(area):
	Entered_bully_area = true

func _on_Fade_finished():
	$Position2D/Camera2D/AcceptDialog.popup()
	player.canMove = true

func scene(key):
	
	match key:
		0:
			$day1.hide()
			if camera_moved and Entered_bully_area:
				fade.rect_position.x = $Position2D.position.x - 40
				scene_index+=1
				$Timer.start(0)
			$Position2D.position.x = clamp($Position2D.position.x, 0, walkable_area_limit-1440)
			$Player.position.x = clamp($Player.position.x, 0, walkable_area_limit)
			update_camera_postion(camera_speed)
		
		1:
			player.canMove = false
			match round($Timer.time_left):
					8.0:
						enemy_dialogue.start(0,1)
					5.0: 
						player.speak_dialogue(1,2)
					1.0:
						fadeDelay.start(0)
		2:
			control.mode = 1
			player.speed = 200
			if $"Fight Music".playing == false:
				$"Fight Music".play(0)
			player.position.x = clamp($Player.position.x, left_Limit, walkable_area_limit)
			$Position2D.position.x = clamp($Position2D.position.x, left_Limit - 1440, walkable_area_limit-1440)
			player.actionState = 2
			Enemy.active(true)
			control.update_health(player.health,Enemy.health)
			$bully_area/CollisionShape2D.disabled = true
			
			if player.health == 0:
				global.gameOver = true
				player.canMove = false
				Enemy.canMove = false
			if Enemy.health == 0:
				$"Fight Music".stop()
				fade.start()
				scene_index+=1
				Enemy.position = Vector2(3879.41,442.133)
				player.position = Vector2(3979.41,480.524)
				
		3: 
			player.speed = 75
			player.position.x = clamp($Player.position.x, left_Limit, walkable_area_limit)
			control.mode = 2
			player.actionState = 1
			Enemy.runaway()
			$"Pickable Item4".position = Vector2(3579.41,442.133)
			$"Pickable Item4".show()
			scene_index+=1
			$"Change Scene/CollisionShape2D".disabled = false
		4:
			player.position.x = clamp($Player.position.x, left_Limit, walkable_area_limit)


func fightscene_reset():
	player.health = 100
	Enemy.health = 100
	player.canMove = true
	Enemy.canMove = true
	global.gameOver = false
	player.position = Vector2(3172.41,480.524)
	Enemy.position = Vector2(3979.41,442.133)

func _on_Change_Scene_body_entered(body):
	$Fade2.show()
	$Fade2.start()
	$Player.canMove = false

func _on_Fade2_finished():
	get_tree().change_scene("res://Scenes/Day_1/Junkshop.tscn")


func _on_GameOver_game_over():
	fightscene_reset()
