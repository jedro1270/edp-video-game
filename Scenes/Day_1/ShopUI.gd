extends Node2D

signal bought

func _on_OptionButton_bought():
	emit_signal("bought")


func _on_Area2D_area_entered(area):
	$"ShopWindow".show()


func _on_Area2D_area_exited(area):
	$"ShopWindow".hide()
