extends Node2D

signal win
signal lose

var fade = false

var won = false

func _on_FightingPlayer_playerhit():
	$healthbar_player.value -= 10
	if $healthbar_player.value <= 0:
		_fade()
		emit_signal("lose")

func _on_FightingPlayer_enemyhit():
	$healthbar_enemy.value -= 100 # for testing
	if $healthbar_enemy.value <= 0:
		_fade()
		emit_signal("win")
		
func _fade():
	if fade == false:
		get_parent().add_child(load("res://Scenes/Fade.tscn").instance())
		fade = true
