extends KinematicBody2D

var direction = -1
var time = 0
export var speed = 250

func _physics_process(delta):
	var velocity = Vector2()
	time+=delta

	velocity.x += direction
	runAnimation(velocity)
	velocity = velocity.normalized() * speed
	
	position += velocity * delta

func runAnimation(velocity:Vector2):
	$AnimatedSprite.animation = "walking"
	$AnimatedSprite.play()
