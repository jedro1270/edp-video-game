extends Node2D

func _ready():
	$FadeIn.show()
	$FadeIn.start()

func _on_entering_junkShop_area_entered(area):
	$Player.canMove = false
	$Fade.show()
	$Fade.start()
	

func _on_Fade_finished():
	get_tree().change_scene("res://Scenes/Day_1/Junkshop_After.tscn")


func _on_Player_Detector_body_entered(body):
	get_tree().change_scene("res://Scenes/Day_1/Landfill scavenge.tscn")
