extends Node2D

func _ready():
	$Fade.show()
	$Fade.start()
	$"Player Dialogue".hide()
	$"Player detector/CollisionShape2D".disabled = true
	if global.bought_food == true:
		$"Player detector/CollisionShape2D".disabled = false
		$Sister/AnimatedSprite.animation = "sit"
	else:
		$Sister/AnimatedSprite.animation = "sleep"

func _on_Player_Dialogue_end():
	$FadeOut.show()
	$FadeOut.start()
	$Timer.start(0)

func _on_Player_detector_body_entered(body):
	$"Player Dialogue".show()
	$"Player Dialogue".start(0, 2)
	$Player.canMove = false

func _on_Timer_timeout():
	get_tree().change_scene("res://Scenes/Day_2/Day_2_Morning.tscn")
