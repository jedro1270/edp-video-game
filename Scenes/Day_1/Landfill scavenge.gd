extends Node2D

func _ready():
	$CanvasLayer/Fade.show()
	$CanvasLayer/Fade.start()
	$Player/AnimatedSprite.flip_h = true


func _on_No_Entry_body_entered(body):
	$Player.speak_dialogue(0, 1)
