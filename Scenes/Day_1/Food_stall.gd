extends Node2D


func _ready():
	$"Shop/ShopWindow".hide()
	$"Change Scene/CollisionShape2D".disabled = true
	$Fade.show()
	$Fade.start()

func _on_Shop_bought():
	$"Change Scene/CollisionShape2D".disabled = false
	$"Change Scene2/CollisionShape2D".disabled = true
	$Shop.queue_free()
	$"Cutscene Dialogue".show()
	$"Cutscene Dialogue".start(0, 2)
	$Player.canMove = false

func _on_Cutscene_Dialogue_end():
	$Player.canMove = true
