extends Area2D

signal collected_items

export(String) var tag = 'Item'
export(Texture) var texture
var bottles = 10
var newspaper = 5
var food = 0

func _ready():
	$Sprite.texture = texture 
	var playerNode = get_parent().get_node("Player")
	self.connect("collected_items",playerNode,"update_Collected_Items",[ bottles, newspaper])

func _on_Player_Action():
	for body in get_overlapping_bodies():
		if body.name == 'Player':
			$"Pick Up Sound".play()
			emit_signal("collected_items")
			queue_free()
