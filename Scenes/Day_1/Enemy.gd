extends KinematicBody2D

signal Punch(damage, facing)

var velocity = Vector2()
var player_position = Vector2()
var runaway_stat = false
var facing = 0
var playerNearby := false
var canPunch := true
var canMove = true
export var speed  = 5
export var damage = 10
export var health = 100
onready var pivot = $Pivot
onready var punch = $Pivot/punch/Enemypunch
onready var punchTimer =$"Punch timer"

func _ready():
	punch.disabled = true
	punchTimer.stop()
	set_physics_process(false)

func active(status):
	set_physics_process(status)

func runaway():
	$"Punch timer".queue_free()
	speed = 10
	runaway_stat = true

func _physics_process(delta):
	if canMove == true:
		velocity = Vector2()
		
		if runaway_stat:
			velocity.x -= 10
		else:
			velocity.x += track_player()
		
		if velocity .length() > 0:
			velocity = velocity.normalized()*speed
			punch.disabled = true
			playerNearby = false
			
		if canPunch && playerNearby:
			punchTimer.start()
			canPunch = false
			
		runAnimation(velocity)
		move_and_collide(velocity)
		position += velocity * delta

func runAnimation(velocity:Vector2):
	if velocity.x > 0 :
		$AnimatedSprite.play("walking")
		$AnimatedSprite.flip_h = false
		pivot.rotation_degrees = 180
	elif velocity.x < 0:
		$AnimatedSprite.play("walking")
		$AnimatedSprite.flip_h = true
		pivot.rotation_degrees = 0
	else:
		$AnimatedSprite.stop()

func track_player():
	
	var playerLocation =global.player.location
	var myLocation = self.position.x
	var displacment_to_player =  playerLocation - myLocation
	var direction = 0
	var offset = 15
	
	if displacment_to_player < -offset:
		direction = -1
		facing = -1
	elif displacment_to_player > offset:
		direction = 1
		facing = 1
	else:
		direction = 0
		playerNearby = true
	return direction as float

func _on_Player_Punch(damage:int):
	health-=damage
	$"Enemy Hit Sound".play()
	print("player hit enemy! " + String(health))
	translate(Vector2(50*-facing, 0))
	
func _on_punch_body_entered(body):
	if body.name == "Player":
		emit_signal("Punch",damage, facing)

func _on_Area2D_body_entered(body):
	if body.name == "Player" and runaway_stat == false:
		punchTimer.start()

func _on_Punch_timer_timeout():
	canPunch = true
	punch.disabled = false
	$AnimatedSprite.play("punch")
