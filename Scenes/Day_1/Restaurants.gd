extends Node2D

var isPlayerDialoguePlayed = false
var isNPCDialoguePlayed = false

func _ready():
	$Food.hide()
	$Fade.show()
	$Fade.start()
	$"Pickable Item/CollisionShape2D".disabled = true

func _on_NPC_Dialogue_Trigger_body_entered(body):
	if isNPCDialoguePlayed == false:
		isNPCDialoguePlayed = true
		$"NPC Dialogue".show()
		$"NPC Dialogue".start(0, 2)
		
func _on_Change_Scene_body_entered(body):
	if global.bought_food == false:
		$"Player/Camera2D/Cutscene Dialogue".start(2, 3)
	else:
		$Player/AnimatedSprite.animation = "idle"
		$"Player".canMove = false
		$Fade2.show()
		$Fade2.start()

func _on_Player_dialogue_end():
	$Food.show()
	$"Pickable Item/CollisionShape2D".disabled = false

func _on_Dialogue_trigger_body_entered(body):
	if isPlayerDialoguePlayed == false: # So that it will only execute once
		isPlayerDialoguePlayed = true
		$"Player".speak_dialogue(0, 1)
		$"Player/Player Dialogue/Speech Bubble/Player Dialogue Text/Character Timer".start(0)


func _on_Pickable_Item_collected_items():
	global.bought_food = true
	global.player.foodServing += 1
	$Food.queue_free()
	$"Player/Camera2D/Cutscene Dialogue".start(0, 2)
	$Player.canMove = false
	

func _on_Cutscene_Dialogue_end():
	$Player.canMove = true


func _on_Fade2_finished():
	get_tree().change_scene("res://Scenes/Day_1/Landfill_home_right.tscn")


func _on_Player_Detector_body_entered(body):
	$"Player/Camera2D/Cutscene Dialogue".start(3, 5)
