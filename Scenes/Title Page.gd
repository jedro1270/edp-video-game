extends Node2D

var savePath = "user://savegame.save"
var next_scene = ""

func _ready():
	global.player.collected_Items.bottles = 0
	global.player.collected_Items.newspapers = 0
	global.player.foodServing = 0
	global.player.money = 0

func _on_New_Game_pressed():
	$ConfirmationDialog.popup_centered()
	

func _on_Exit_pressed():
	get_tree().quit()

func _on_Fade_finished():
	get_tree().change_scene(next_scene)

func _on_Load_Game_pressed():
	global.load_scene()

func _on_Learn_More_pressed():
	OS.shell_open("https://www.un.org/en/sections/issues-depth/poverty/")

func _on_Credits_pressed():
	$Fade.show()
	$Fade.start()
	$"Fade Music".fade_out($Music)
	next_scene = "res://Scenes/Credits.tscn"


func _on_ConfirmationDialog_confirmed():
	$Fade.show()
	$Fade.start()
	$"Fade Music".fade_out($Music)
	next_scene = "res://Scenes/Day_1/Introduction Video.tscn"
